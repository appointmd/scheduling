﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAgendaByIdQueryHandler : IRequestHandler<GetAgendaByIdQuery, AgendaResponse>
    {
        private readonly IAgendasRepository _agendasRepository;
        public GetAgendaByIdQueryHandler(IAgendasRepository agendasRepository)
        {
            _agendasRepository = agendasRepository;
        }
        public async Task<AgendaResponse> Handle(GetAgendaByIdQuery request, CancellationToken cancellationToken)
        {
            var agenda = await _agendasRepository.GetAgenda(request.Id);
            var agendaResponses = AgendaMapper.Mapper.Map<AgendaResponse>(agenda);
            return agendaResponses;
        }
    }
}
