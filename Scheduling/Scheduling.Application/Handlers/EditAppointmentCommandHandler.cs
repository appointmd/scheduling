﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;

namespace Scheduling.Application.Handlers
{
    public class EditAppointmentCommandHandler : IRequestHandler<EditAppointmentCommand, bool>
    {
        private readonly IAppointmentsRepository _appointmentsRepository;

        public EditAppointmentCommandHandler(IAppointmentsRepository appointmentsRepository)
        {
            _appointmentsRepository = appointmentsRepository;
        }

        public async Task<bool> Handle(EditAppointmentCommand request, CancellationToken cancellationToken)
        {
            var appointmentEntity = await _appointmentsRepository.EditAppointment(new Appointment
            {
                Id = request.Id,
                Name = request.Name,
                Status = request.Status,
                PlannedForDate = request.PlannedForDate,
                AgendaId = request.AgendaId
            }) ;

                 return appointmentEntity;
        }

       
    }
}
