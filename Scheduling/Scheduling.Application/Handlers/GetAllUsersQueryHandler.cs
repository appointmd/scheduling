﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, IList<UserResponse>>
    {
        private readonly IUsersRepository _usersRepository;

        public GetAllUsersQueryHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }
        public async Task<IList<UserResponse>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _usersRepository.GetUsers();
            var userResponses = AgendaMapper.Mapper.Map<IList<UserResponse>>(users);
            return userResponses;
        }
    }
}
