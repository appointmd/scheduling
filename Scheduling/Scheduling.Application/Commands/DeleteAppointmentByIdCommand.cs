﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR; 

namespace Scheduling.Application.Commands
{
    public class DeleteAppointmentByIdCommand : IRequest<bool>
    {
        public string Id { get; set; }

        public DeleteAppointmentByIdCommand(string id)
        {
            Id = id;
        }
    }
}
