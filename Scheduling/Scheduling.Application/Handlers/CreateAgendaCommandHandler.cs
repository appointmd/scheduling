﻿using Amazon.Runtime.Internal;
using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Application.Mappers;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class CreateAgendaCommandHandler : IRequestHandler<CreateAgendaCommand, AgendaResponse>
    {
        private readonly IAgendasRepository _agendasRepository;

        public CreateAgendaCommandHandler(IAgendasRepository agendasRepository)
        {
            _agendasRepository = agendasRepository;
        }

        public async Task<AgendaResponse> Handle(CreateAgendaCommand request, CancellationToken cancellationToken)
        {
            var agendaEntity = AgendaMapper.Mapper.Map<Agenda>(request);
            if(agendaEntity is null)
            {
                throw new ApplicationException("There is an issue with mapping while creating new agenda.");
            }

            var newAgenda = await _agendasRepository.CreateAgenda(agendaEntity);
            var agendaResponse = AgendaMapper.Mapper.Map<AgendaResponse>(newAgenda);
            return agendaResponse;
        }
    }
}
