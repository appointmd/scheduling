﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAgendaByNameQueryHandler : IRequestHandler<GetAgendaByNameQuery, IList<AgendaResponse>>
    {
        private readonly IAgendasRepository _agendasRepository;
        public GetAgendaByNameQueryHandler(IAgendasRepository agendasRepository)
        {
            _agendasRepository = agendasRepository;
        }
        public async Task<IList<AgendaResponse>> Handle(GetAgendaByNameQuery request, CancellationToken cancellationToken)
        {
            var agendas = await _agendasRepository.GetAgendaByName(request.Name);
            var agendaResponses = AgendaMapper.Mapper.Map<IList<AgendaResponse>>(agendas);
            return agendaResponses;
        }
    }
}
