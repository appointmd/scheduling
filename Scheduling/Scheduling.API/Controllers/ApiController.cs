﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;

namespace Scheduling.API.Controllers
{
    [ApiVersion("1")]
    [Microsoft.AspNetCore.Mvc.Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class ApiController : ControllerBase
    {
    }
}
