﻿using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class DeleteAgendaByIdHandler : IRequestHandler<DeleteAgendaByIdCommand, bool>
    {
        private readonly IAgendasRepository _agendasRepository;

        public DeleteAgendaByIdHandler(IAgendasRepository agendasRepository)
        {
            _agendasRepository = agendasRepository;
        }
        public async Task<bool> Handle(DeleteAgendaByIdCommand request, CancellationToken cancellationToken)
        {
            return await _agendasRepository.DeleteAgenda(request.Id);
        }
    }
}
