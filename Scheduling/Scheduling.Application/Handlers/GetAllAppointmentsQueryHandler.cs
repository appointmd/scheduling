﻿using AutoMapper;
using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAllAppointmentsQueryHandler : IRequestHandler<GetAllAppointmentsQuery, IList<AppointmentResponse>>
    {
        private readonly IAppointmentsRepository _appointmentRepository;
   

        public GetAllAppointmentsQueryHandler(IAppointmentsRepository appointmentsRepository)
        {
            _appointmentRepository = appointmentsRepository;
        
        }
        public async Task<IList<AppointmentResponse>> Handle(GetAllAppointmentsQuery request, CancellationToken cancellationToken)
        {
            var appointments = await _appointmentRepository.getAllAppoinments();
            var appointmentsResponse = AgendaMapper.Mapper.Map<IList<Appointment>, IList<AppointmentResponse>>(appointments.ToList());
            return appointmentsResponse;
        }
    }
}
