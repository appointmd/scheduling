﻿using Scheduling.Core.Entities;
using Scheduling.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Repositories
{
    public interface IAgendasRepository
    {
        Task<Pagination<Agenda>> GetAgendas(SchedulingSpecificationParameters schedulingSpecificationParameters);
        Task<IEnumerable<Agenda>> GetAgendaByName(string name);
        Task<IEnumerable<Agenda>> GetAgendaByUser(string name);
        Task<Agenda> GetAgenda(string id);

        Task<Agenda> CreateAgenda(Agenda agenda);
        Task<bool> UpdateAgenda(Agenda agenda);
        Task<bool> DeleteAgenda(string id);
    }
}
