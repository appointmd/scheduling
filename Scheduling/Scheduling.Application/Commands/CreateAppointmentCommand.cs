﻿using Amazon.Runtime.Internal;
using Scheduling.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Scheduling.Application.Commands
{
    public class CreateAppointmentCommand : IRequest<AppointmentResponse>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime PlannedForDate { get; set; }
        public string AgendaId { get; set; }
    }
}
