﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Specifications
{
    public class SchedulingSpecificationParameters
    {

        private const int MaxPageSize = 25;
        public int PageIndex {get;set; } = 1;
        private int _pageSize = 10;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        public string? AgendaId { get; set; }
        public string? AppointmentId { get; set; }
        public string? UserId { get; set; }
        public string? Sort { get; set; }

        public string? Search { get; set; }

    }
}
