﻿using MongoDB.Driver;
using Newtonsoft.Json;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;


namespace Scheduling.Infrastructure.Data
{
    public static class AgendaContextSeeder
    {
        public static void SeedData(IMongoCollection<Agenda> agendaCollection)
        {
            
            // check if seeded
            bool checkAgendas = agendaCollection.Find(a => true).Any();
            
            string path = Path.Combine("Data", "SeedData", "agendas.json");
            // if not seeded, check datafile and seed
            if (!checkAgendas)
            {
                var agendaData = File.ReadAllText(path);
                var agendas = JsonConvert.DeserializeObject<List<Agenda>>(agendaData);
                if (agendas != null)
                {
                    foreach (var agenda in agendas)
                    {
                        agendaCollection.InsertOneAsync(agenda);
                    }
                }
            }
        }
    }
}
