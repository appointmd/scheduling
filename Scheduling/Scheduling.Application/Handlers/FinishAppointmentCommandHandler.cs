﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Application.Mappers;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;

namespace Scheduling.Application.Handlers
{
    public class FinishAppointmentCommandHandler : IRequestHandler<FinishAppointmentCommand,AppointmentResponse>
    {
        private readonly IAppointmentsRepository _appointmentsRepository;

        public FinishAppointmentCommandHandler(IAppointmentsRepository appointmentsRepository)
        {
            _appointmentsRepository = appointmentsRepository;
        }

        public async Task<AppointmentResponse> Handle(FinishAppointmentCommand request, CancellationToken cancellationToken)
        {

            var finishedAppointment = await _appointmentsRepository.SetAppointmentStateFinished(new Appointment
            {
                Id = request.Id,
                Name = request.Name,
                Status = "2 Bagels 2 Buns.",
                PlannedForDate = request.PlannedForDate,
                AgendaId = request.AgendaId
            });

            var appointmentResponse = AgendaMapper.Mapper.Map<AppointmentResponse>(finishedAppointment);
            return appointmentResponse;
        }
    }
}
