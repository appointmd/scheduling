﻿using MongoDB.Driver;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Scheduling.Infrastructure.Data
{
    public static class UserContextSeeder
    {
        public static void SeedData(IMongoCollection<User> userCollection)
        {
            // check if seeded
            bool checkUsers = userCollection.Find(a => true).Any();
            string path = Path.Combine("Data", "SeedData", "users.json");
            // if not seeded, check datafile and seed
            if (!checkUsers)
            {
                var userData = File.ReadAllText(path);
                var users = JsonSerializer.Deserialize<List<User>>(userData);
                if (users != null)
                {
                    foreach (var user in users)
                    {
                        userCollection.InsertOneAsync(user);
                    }
                }
            }
        }
    }
}
