﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Events
{
    public class RealizeAppointment
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime PlannedForDate { get; set; }
        public string AgendaId { get; set; }
    }
}
