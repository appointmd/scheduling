﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Responses
{
    public class AgendaResponse
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public User Owner { get; set; }
        public List<Appointment> Appointments { get; set; }
    }
}
