﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAppointmentByIdQueryHandler : IRequestHandler<GetAppointmentByIdQuery, AppointmentResponse>
    {
        private readonly IAppointmentsRepository _appointmentsRepsitory;

        public GetAppointmentByIdQueryHandler(IAppointmentsRepository appointmentsRepsitory)
        {
            _appointmentsRepsitory = appointmentsRepsitory;
        }

        public async Task<AppointmentResponse> Handle(GetAppointmentByIdQuery request, CancellationToken cancellationToken)
        {
            var appointment = await _appointmentsRepsitory.GetAppointmentById(request.Id);
            var appointmentResponse = AgendaMapper.Mapper.Map<AppointmentResponse>(appointment);
            return appointmentResponse;
        }
    }
}
