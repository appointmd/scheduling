﻿using MediatR;
using Scheduling.Application.Commands;
using Scheduling.Application.Mappers;
using Scheduling.Application.Responses;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class CreateAppointmentCommandHandler : IRequestHandler<CreateAppointmentCommand, AppointmentResponse>
    {
        private readonly IAppointmentsRepository _appointementsRepository;

        public CreateAppointmentCommandHandler(IAppointmentsRepository appointementsRepository)
        {
            _appointementsRepository = appointementsRepository;
        }

        public async Task<AppointmentResponse> Handle(CreateAppointmentCommand request, CancellationToken cancellationToken)
        {
            var appointmentEntity = AgendaMapper.Mapper.Map<Appointment>(request);
            if(appointmentEntity is null)
            {
                throw new ApplicationException("Problem occured while creating new appointment.");
            }

            var newAppointment = await _appointementsRepository.CreateAppointment(appointmentEntity);
            var appointmentResponse = AgendaMapper.Mapper.Map<AppointmentResponse>(newAppointment);
            return appointmentResponse;
        }
    }
}
