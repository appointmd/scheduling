﻿using Amazon.Runtime.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Scheduling.Application.Commands
{
    public class DeleteAgendaByIdCommand : IRequest<bool>
    {
        public string Id { get; set; }

        public DeleteAgendaByIdCommand(string id)
        {
            Id = id;
        }
    }
}
