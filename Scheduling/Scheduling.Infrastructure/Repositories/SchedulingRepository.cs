﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Scheduling.Core.Entities;
using Scheduling.Core.Repositories;
using Scheduling.Core.Specifications;
using Scheduling.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Infrastructure.Repositories
{
    public class SchedulingRepository : IAgendasRepository, IAppointmentsRepository, IUsersRepository
    {
        private readonly IScheduleContext _context;
        public SchedulingRepository(IScheduleContext scheduleContext)
        {
            _context = scheduleContext;
        }
        public async Task<Agenda> CreateAgenda(Agenda agenda)
        {
            await _context.Agendas.InsertOneAsync(agenda);
            return agenda;
        }

        public async Task<bool> UpdateAgenda(Agenda agenda)
        {
            var updateResult = await _context
                .Agendas
                .ReplaceOneAsync(a => a.Id == agenda.Id, agenda);

            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> DeleteAgenda(string id)
        {
            FilterDefinition<Agenda> filter = Builders<Agenda>.Filter.Eq(a => a.Id, id);
            DeleteResult deleteResult = await _context
                .Agendas
                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<Agenda> GetAgenda(string id)
        {
            return await _context.Agendas.Find(a => a.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Appointment> GetAppointmentById(string id)
        {
            return await _context.Appointments.Find(a => a.Id == id).FirstOrDefaultAsync(); 
        }


        public async Task<Pagination<Agenda>> GetAgendas(SchedulingSpecificationParameters schedulingSpecificationParameters)
        {
            var builder = Builders<Agenda>.Filter;
            var filter = builder.Empty;
            if (!string.IsNullOrEmpty(schedulingSpecificationParameters.Search))
            {
                var searchFilter = builder.Regex(x => x.Name, new MongoDB.Bson.BsonRegularExpression(schedulingSpecificationParameters.Search));
                filter &= searchFilter;
            }

            // halted. I am in a rush, don't want to put too much time in this.
           /* if (!string.IsNullOrEmpty(schedulingSpecificationParameters.AppointmentId))
            {

                var userFilter = builder.Regex(x => x.Appointments., new MongoDB.Bson.BsonRegularExpression(schedulingSpecificationParameters.AppointmentId));
                filter &= userFilter;
            }*/

            // not working, not going to spend time yet.
            if (!string.IsNullOrEmpty(schedulingSpecificationParameters.UserId))
            {
                var userFilter = builder.Eq(x => x.Id, schedulingSpecificationParameters.UserId);
                filter &= userFilter;
            }

            if(!string.IsNullOrEmpty(schedulingSpecificationParameters.Sort))
            {
                return new Pagination<Agenda>
                {
                    PageSize = schedulingSpecificationParameters.PageSize,
                    PageIndex = schedulingSpecificationParameters.PageIndex,
                    Data = await _context.Agendas.Find(filter)
                    .Sort(Builders<Agenda>.Sort.Ascending("Name"))
                    .Skip(schedulingSpecificationParameters.PageIndex - 1)
                    .Limit(schedulingSpecificationParameters.PageSize)
                    .ToListAsync(),
                    Count = await _context.Agendas.CountDocumentsAsync(p => true) // apply UI
                };
            }

            return new Pagination<Agenda>()
            {
                PageSize = schedulingSpecificationParameters.PageSize,
                PageIndex = schedulingSpecificationParameters.PageIndex,
                Data = await _context.Agendas
                    .Find(filter)
                    .Sort(Builders<Agenda>.Sort.Ascending("Name"))
                    .Skip(schedulingSpecificationParameters.PageSize * (schedulingSpecificationParameters.PageIndex - 1))
                    .Limit(schedulingSpecificationParameters.PageSize).ToListAsync(),
                Count = await _context.Agendas.CountDocumentsAsync(p => true)
            };
        }

        private async Task<IReadOnlyList<Agenda>> DataFilter(SchedulingSpecificationParameters schedulingSpecificationParameters, FilterDefinition<Agenda> filter)
        {
            // No sort situations yet. For now only alphabetically sorted
            switch(schedulingSpecificationParameters.Sort)
            {
                 
                default:
                    return await _context.Agendas.Find(filter).Sort(Builders<Agenda>.Sort.Ascending("Name"))
                    .Skip(schedulingSpecificationParameters.PageSize * (schedulingSpecificationParameters.PageIndex - 1))
                    .Limit(schedulingSpecificationParameters.PageSize).ToListAsync();
            }    
        }

        public async Task<IEnumerable<Agenda>> GetAgendaByName(string name)
        {
            FilterDefinition<Agenda> filter = Builders<Agenda>.Filter.Eq(a => a.Name, name);
            return await _context
                .Agendas
                .Find(filter)
                .ToListAsync();
        }

      
        public async Task<IEnumerable<Agenda>> GetAgendaByUser(string name)
        {
            FilterDefinition<Agenda> filter = Builders<Agenda>.Filter.Eq(a => a.Owner.Name, name);
            return await _context
                .Agendas
                .Find(filter)
                .ToListAsync();
        }

        public async Task<IEnumerable<Appointment>> getAllAppoinments()
        {
            return await _context
                .Appointments
                .Find(a => true)
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _context
                .Users
                .Find(u => true)
                .ToListAsync();
        }

        public async Task<Appointment> CreateAppointment(Appointment appointment)
        {
            await _context.Appointments.InsertOneAsync(appointment);
            return appointment;
        }

        public async Task<bool> EditAppointment(Appointment appointment)
        {
            var updateResult = await _context
               .Appointments
               .ReplaceOneAsync(a => a.Id == appointment.Id, appointment);

            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> CancelAppointment(string id)
        {
            FilterDefinition<Appointment> filter = Builders<Appointment>.Filter.Eq(a => a.Id, id);
            DeleteResult deleteResult = await _context
                .Appointments
                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }


        // TODO: Return full appointment entity -> send status from frontend 
        public async Task<Appointment> SetAppointmentStateFinished(Appointment appointment)
        {
            var filter = Builders<Appointment>.Filter.Eq(
                a => a.Id, appointment.Id);

            ReplaceOneResult replaceOneResult = await _context.Appointments.ReplaceOneAsync(filter, appointment);
        
            return appointment;
        }

      

        // MOD: Changes status in backend     
        /*   public async Task<bool> SetAppointmentStateFinished(string id)
           {
               var filter = Builders<Appointment>.Filter
               .Eq(a => a.Id, id);

               var update = Builders<Appointment>.Update
                   //TODO: Change to Finished, I just thought this was really funny
                 .Set(a => a.Status, "2 Bagels 2 Buns");

               UpdateResult result = await _context.Appointments.UpdateOneAsync(filter, update);

               return result.IsAcknowledged && result.ModifiedCount > 0;
           }*/
    }
}
