﻿using MediatR;
using Scheduling.Application.Mappers;
using Scheduling.Application.Queries;
using Scheduling.Application.Responses;
using Scheduling.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Application.Handlers
{
    public class GetAgendaByUserQueryHandler: IRequestHandler<GetAgendaByUserQuery, IList<AgendaResponse>>
    {
        private readonly IAgendasRepository _agendaRepository;
        public GetAgendaByUserQueryHandler(IAgendasRepository agendaRepository)
        {
            _agendaRepository = agendaRepository;
        }

        public async Task<IList<AgendaResponse>> Handle(GetAgendaByUserQuery request, CancellationToken cancellationToken)
        {
            var agendas = await _agendaRepository.GetAgendaByUser(request.owner);
            var agendaResponses = AgendaMapper.Mapper.Map<IList<AgendaResponse>>(agendas);
            return agendaResponses;
        }
    }
}
