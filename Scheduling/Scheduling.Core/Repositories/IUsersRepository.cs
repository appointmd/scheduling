﻿using Scheduling.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling.Core.Repositories
{
    public interface IUsersRepository
    {
        // users that have a agenda
        Task<IEnumerable<User>> GetUsers();
    }
}
